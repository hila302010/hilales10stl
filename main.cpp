#include"Customer.h"
#include <iostream>
#include<map>
#define EXIT 4
#define SIZE_ITEMS 10
void printMenu();
// option 1
void newCustomer(Item itemList[SIZE_ITEMS], map<string, Customer> & abcCustomers);
// option 2
void updateCustomersList(Item itemList[SIZE_ITEMS], map<string, Customer> & abcCustomers);
// option 3
void maxPayer(map<string, Customer>  customers);

void printItems(Item itemList[SIZE_ITEMS]);
void addOrEraseItems(Customer & newCustomer, Item itemList[SIZE_ITEMS], bool addOrErase);

int main()
{
	int choice = 0;
	// map of customers
	map<string, Customer> abcCustomers;
	// list of items
	Item itemList[SIZE_ITEMS] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
	while (choice != EXIT)
	{
		// print the menu
		printMenu();
		// check the choice
		cin >> choice;
		getchar();
		switch (choice)
		{
		case 1:
			newCustomer(itemList, abcCustomers);
			break;
		case 2:
			updateCustomersList(itemList, abcCustomers);
			break;
		case 3:
			maxPayer(abcCustomers);
			break;
		case 4:
			cout << "BYE :) \n\n";
			break;
		default:
			break;
		}

	}
	getchar();
	return 0;
}

void printMenu()
{
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1.      to sign as customer and buy items" << endl;
	cout << "2.      to uptade existing customer's items" << endl;
	cout << "3.      to print the customer who pays the most" << endl;
	cout << "4.      to exit" << endl;
}
// option 1
void newCustomer(Item itemList[SIZE_ITEMS], map<string, Customer> &  abcCustomers)
{
	string nameCustomer; // of customer
	string nameItem;
	int indexItem;
	Customer newCustomer = Customer();
	cout << "Dear customer, enter your name: ";
	cin >> nameCustomer;
	newCustomer.setName(nameCustomer);
	// check if customer exist
	if ((abcCustomers.find(nameCustomer)) != abcCustomers.end()) 
	{
		cout << "ERROR - Customer all ready exist." << endl;
		return;
	}
	printItems(itemList);
	addOrEraseItems(newCustomer, itemList, true);
	// add a customer
	abcCustomers.insert(std::pair<string, Customer>(nameCustomer, newCustomer));
}
// option 2
void updateCustomersList(Item itemList[SIZE_ITEMS], map<string, Customer> & abcCustomers)
{
	string name; // of customer
	string nameItem;
	int choice = 0;
	cout << "Dear customer, enter your name: ";
	cin >> name;
	// check if customer exist
	if ((abcCustomers.find(name)) == abcCustomers.end())
	{
		cout << "ERROR - Customer doesn't exist." << endl;
		return;
	}
	while (choice != 3)
	{
		cout << "1.      Add items" << endl;
		cout << "2.      Remove items" << endl;
		cout << "3.      Back to menu" << endl;
		cin >> choice;
		getchar();
		switch (choice)
		{
		case 1:
			printItems(itemList);
			addOrEraseItems(abcCustomers[name], itemList, true); // true to add items
			choice = 3;
			break;
		case 2:
			printItems(itemList);
			addOrEraseItems(abcCustomers[name], itemList, false); // false to remove items
			choice = 3;
			break;
		case 3:
			cout << "Back to menu\n\n";
			break;
		default:
			cout << "ERROR. only 1-3 is allowed. try again\n";
			break;
		}
	}
}
// option 3
void maxPayer(map<string, Customer> customers)
{
	double maxPrice = 0;
	Customer _maxPayer;
	for (auto it = customers.cbegin(); it != customers.cend(); ++it){
		double totalPrice = it->second.totalSum();
		if (totalPrice > maxPrice) {
			maxPrice = totalPrice;
			_maxPayer = it->second;
		}
	}
	cout << "Customer who pays the most: " << _maxPayer.getName() << ". his price: " << maxPrice << "\n\n";
}

// print items:
void printItems(Item itemList[SIZE_ITEMS])
{
	cout << "\nThe items you can buy are :" << endl;
	for (int i = 0; i < SIZE_ITEMS; i++)
	{
		cout << i + 1 << ". " << itemList[i].getName() << ",	price: " << itemList[i].getUnitPrice() << endl;
	}
}

// indox items from customer:
void addOrEraseItems(Customer & newCustomer, Item itemList[SIZE_ITEMS], bool addOrErase)
{
	int indexItem;
	cout << "\nEnter your items : (0 to exit)" << endl;
	do
	{
		cout << "What item would you like to buy? (1-10) ";
		cin >> indexItem;
		if (indexItem <= SIZE_ITEMS && indexItem >= 1)
			if (addOrErase == true)
				newCustomer.addItem(itemList[indexItem - 1]);
			else if (addOrErase == false)
				newCustomer.removeItem(itemList[indexItem - 1]);
		else if (indexItem != 0)
			cout << "Item is not in the menu. try again" << endl;
	} while (indexItem != 0);
}


