#pragma once
#include<iostream>
#include<string>
#include<algorithm>

using namespace std;

class Item
{
public:
	// ctor
	Item();
	Item(string name, string serialNumber, double unitPrice);
	// dtor
	~Item();
	// getters
	string getName();
	string getserialNumber();
	double getUnitPrice();
	int getCount();
	// setters
	void setName(string name);
	void setserialNumber(string serialNumber);
	void setUnitPrice(double unitPrice);
	void setCount(int count);

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

											   //get and set functions

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};
