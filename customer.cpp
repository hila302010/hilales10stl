#include "customer.h"
// ctors
Customer::Customer()
{
}

Customer::Customer(string name, Item item)
{
	_name = name;
	addItem(item);
}
// dtor
Customer::~Customer()
{
}
// getters
string Customer::getName()
{
	return _name;
}

set<Item> Customer::getItems()
{
	return _items;
}
// setters
void Customer::setName(string name)
{
	_name = name;
}

void Customer::setItems(set<Item> items)
{
	_items = items;
}
// returns the total sum for payment
double Customer::totalSum() const
{
	double _totalSum = 0;
	for (auto it = _items.cbegin(); it != _items.cend(); ++it) {
		_totalSum += it->totalPrice();
	}
	return _totalSum;
}
// add item to the set
void Customer::addItem(Item item)
{
	std::set<Item>::iterator it;
	it = _items.find(item);
	if (it != _items.end())
		item.setCount(item.getCount() + 1);
	_items.insert(item);
}
// remove item from the set
void Customer::removeItem(Item item)
{
	std::set<Item>::iterator it;
	it = _items.find(item);
	if (it != _items.end())
		item.setCount(item.getCount() - 1);
	_items.erase(item);
}
