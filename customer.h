#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	// ctors
	Customer();
	Customer(string name, Item item);
	// dtor
	~Customer();
	// get and set functions
	string getName();
	set<Item> getItems();
	void setName(string name);
	void setItems(set<Item> items);
	// returns the total sum for payment
	double totalSum() const; 
	// add item to the set
	void addItem(Item item); 
	// remove item from the set
	void removeItem(Item item); 


private:
	string _name;
	set<Item> _items;
};
