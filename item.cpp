#include "item.h"

// ctors
Item::Item()
{
}

Item::Item(string name, string serialNumber, double unitPrice)
{
	_name = name;
	_serialNumber = serialNumber;
	_unitPrice = unitPrice;
	_count = 1;
}
// dtor
Item::~Item()
{
}

// getters
string Item::getName()
{
	return _name;
}

string Item::getserialNumber()
{
	return _serialNumber;
}

double Item::getUnitPrice()
{
	return _unitPrice;
}

int Item::getCount()
{
	return _count;
}

// setters
void Item::setName(string name)
{
	_name = name;
}

void Item::setserialNumber(string serialNumber)
{
	_serialNumber = serialNumber;
}

void Item::setUnitPrice(double unitPrice)
{
	_unitPrice = unitPrice;
}

void Item::setCount(int count)
{
	_count = count;
}
// returns _count*_unitPrice
double Item::totalPrice() const
{
	return (_count * _unitPrice);
}
// compares the _serialNumber of those items.
bool Item::operator<(const Item & other) const
{
	if (_serialNumber < other._serialNumber)
		return true;
	else
		return false;
}
// compares the _serialNumber of those items.
bool Item::operator>(const Item & other) const
{
	if (_serialNumber > other._serialNumber)
		return true;
	else
		return false;
}
// compares the _serialNumber of those items.
bool Item::operator==(const Item & other) const
{
	if (_serialNumber == other._serialNumber)
		return true;
	else
		return false;
}
